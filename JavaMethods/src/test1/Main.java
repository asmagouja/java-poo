package test1;

public class Main {

	public static void main(String[] args) {
		String prenom1 = "Daniel";
		String prenom2 = "Anais";
		String prenom3 = "Rose";
		int ageDaniel = 7;
		int ageAnais = 14;
		int ageRose = 20;

		ageCondition(prenom1, ageDaniel);
		ageCondition(prenom2, ageAnais);
		ageCondition(prenom3, ageRose);

	}
	
	public static void ageCondition(String nom, int age) {

		if (age > 18) {
			System.out.println(nom +" Adulte");
		} else if (age <= 10) {
			System.out.println(nom +" Enfant");
		} else {
			System.out.println(nom + " Adolescent");

		}
	}
}
