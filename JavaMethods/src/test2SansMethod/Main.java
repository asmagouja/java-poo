package test2SansMethod;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int mois = scanner.nextInt();
		
		switch (mois) {
		
		case 1:
			System.out.println(" HIVER");
			break;
		case 2:
			System.out.println(" HIVER");
			break;
		case 3:
			System.out.println(" HIVER");
			break;
		case 4:
			System.out.println(" PRINTEMPS");
			break;
		case 5:
			System.out.println(" PRINTEMPS");
			break;
		case 6:
			System.out.println(" PRINTEMPS");
			break;
		case 7:
			System.out.println(" ETE");
			break;
		case 8:
			System.out.println(" ETE");
			break;
		case 9:
			System.out.println("ETE");
			break;
		case 10:
			System.out.println(" AUTUMN");
			break;
		case 11:
			System.out.println(" AUTUMN");
			break;
		case 12:
			System.out.println(" AUTUMN");
			break;
		default:
		    System.out.println(" Faites entrer un entier entre 1 et 12!");
	}
		
	}
}
